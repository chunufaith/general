# Meetup Organizer Template

<!--
Note: this template is intended for organizers who are seeking support from GitLab. If you plan to both be a speaker and an organizer for an event, please use our [meetup-both](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=meetup-both) template. 
-->

## Event Info

Please include the following information about your event: 

- Event/Group Name: 
- Event/Group URL: 
- Organizer Name: 
- Location: 
- Date: 
- Time: 
- Speakers: 
- Description: 

## TODO for Organizer

Please complete the following steps for your event: 

### ASAP 

- [ ] Find speakers. Need help? Use the GitLab [Find a Speaker](https://about.gitlab.com/events/find-a-speaker/) page. 
- [ ] Find a venue. Cafes, community centers, coworking spaces, and local tech companies are common venues for meetups. 
- [ ] Set the date. This requires confirming availability of both venue and speaker.
- [ ] Set up an event page. We are happy to connect your group with our [Meetup page](https://www.meetup.com/pro/gitlab). Leave a comment on this issue if you'd like to leverage our Meetup page to get your group started.  
- [ ] Market the event to your target audience. We recommend Reddit, Twitter, LinkedIn, Facebook, and other social channels to reach your audience. Use appropriate hashtags to reach people outside your network of followers. You may also want to promote the event on tech mailing lists or websites that focus on your area. 
- [ ] Apply a Code of Conduct to your group to ensure the safety and comfort of all participants. [Conference Code of Conduct] is a good option.
 
### One week before your event 
- [ ] Confirm plans for the event with your speaker(s) and venue. This is a good time to discuss how the speakers plan to present. Will they be using their laptop? What type of ports does their laptop have? Will they need an adapter? Do these match what is available at the venue? 
- [ ] Recruit volunteers from your network or community to help you on the day of the event. 
- [ ] Send a reminder to your guests about the event and encourage them to help you promote it. Include a simple ask of "share this with your colleagues, friends, and on social media".

### Day of the event 
- [ ] Send reminder to your guests before 1200 local time. Include directions, an agenda, and any other important information: is the entrance tucked away? will they need an ID or code to enter the building? are folks welcome to arrive early or is there a set time that doors will open? 
- [ ] Print and hang signs directing attendees to the room where the event will be held and the restrooms.
- [ ] Request volunteers arrive 30 min before the start time so you can brief them on their responsibilites and answer any questions. 
- [ ] Test the AV in the room to ensure everything is in working order before guests begin to arrive. This will allow time to troubleshoot should any issues arise. It never hurts to have extra cables, adapters, batteries, etc. 
- [ ] Set up food, drinks, swag, and any other materials you have for the event. 
- [ ] Set up a welcome area so you can sign in guests as they arrive. This is a great role for volunteers. If you do not have guests sign in, please take a headcount during the event. 
- [ ] Welcome your guests, share important updates with the group, review agenda, thank your host, and introduce your speakers. 
- [ ] After the event, make sure you leave the venue clean and return any loaned AV equipment. 
- [ ] Send a thank you to attendees and include a form for feedback. 

## TODO for Evangelist Program Manager 

- [ ] Add to Meetup tracker and [GitLab Events](https://about.gitlab.com/events/) page.  
- [ ] Ship swag (usually stickers and 3 shirts) and ship to speaker, event organizer, or venue. Confirm shipping instructions and audience size before sending. 
- [ ] Submit [issue](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues) to tweet about the event. 
- [ ] Confirm the number of attendees. 
- [ ] Process expenses. 

/label  ~Meetups ~"Evangelist Program"  ~"status:plan" 
/assign @johncoghlan
/due 
