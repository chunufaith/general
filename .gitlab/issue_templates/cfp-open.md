<!--
Title for issue should be "CONFERENCE NAME (due DATE)"
-->

# CFP Info

* Event Name:
* Event Date(s):
* Event Location:  
* CFP Closes:
* CFP page: 
* Suggested topics: 

If you plan to submit a proposal, please add a comment to this issue with a link to your submission and let us know in the #CFP channel on Slack.

Need help with your proposal? @pritianka and @johncoghlan are happy to help if you need assistance.

/label ~"CFP" ~"Speaker Needed" ~"Evangelist Program"  ~"status:plan" 
/assign @johncoghlan 