# Become a GitLab Speaker Template

## Event Information 

Please provide the following information about your interest in becoming a GitLab speaker: 

- Event Name: 
- Event URL: 
- Location: 
- Date: 
- CFP Start Date:
- CFP End Date: 

## Speaker Background 

- Have you given a tech talk before:
- Have you spoke about GitLab before:
- Provide a brief bio that includes your experience with GitLab: 
- Anything else we should know: 

## Support Requested

What type of support are you requesting from GitLab (check all that apply): 

- [ ] Help preparing my CFP 
- [ ] Presentation review 
- [ ] Practice sessions with a GitLabber 
- [ ] Travel assistance

Note: we may not be able to accomodate all requested forms of support for every speaker.

/label ~become-a-speaker  ~"status:plan" 
/assign @johncoghlan
