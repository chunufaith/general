# Community Event Sponsorship Request

<!--
Note: If you are applying this template to an existing issue, please follow these steps: 
1. Copy text from existing issue. 
2. Apply 'sponsorship-request' template to issue. 
3. Paste text from original issue into the appropriate field at the bottom of this template. 
4. Update remaining fields.
-->

## Event Info

Please include the following information about your event: 

- Event/Group Name: 
- Event/Group URL: 
- Organizer Name: 
- Location: 
- Date: 
- Time: 
- Will there be a speaker from the wider GitLab community or a talk about GitLab? 
- Number of attendees: 
- Audience profile (ex: developers, executives, students, non-technical, etc.):
- Prospectus: 

## Event Description

<!-- Use this space to describe your event and include anything important that we forgot to ask. -->

## Type of request

Check all that apply

- [ ] Speaker
- [ ] Sponsorship
- [ ] Swag
 
## Event Scorecard

For detail on how the scorecard is used, please see [How we assess events](https://about.gitlab.com/handbook/marketing/community-relations/evangelist-program/#how-we-assess-requests) in the GitLab Handbook.

| Criteria / Score   | 0 | 1  | 2 | 
|----------------|---------------|---------------|----------------|
| GitLab content | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> |
| Audience type | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> |
| Attendee interaction | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> |
| Event location | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> |
| Event relevance | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> |
| Opportunity size | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> | <ul><li>[ ] </li></ul> |



/label ~"Evangelist Program"  ~"status:plan" 
/assign @johncoghlan
