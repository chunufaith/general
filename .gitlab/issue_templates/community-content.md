# GitLab Community Content Submissions

## Submission Information 

Please provide the following information about your content: 

- Content Title:
- Brief description: 
- URL: 
- Content Type: 
  - [ ] Blog post
  - [ ] Video 
  - [ ] Other


## Submitter Information 

- Name:
- Twitter: 
- Are you the creator/owner of this content:
  - [ ] Yes
  - [ ] No

If no, please provide the following information if possible: 
- Creator/Owner Name:
- Creator/Owner Contact Info: 

/label ~community-content  ~"status:plan" 
/assign @johncoghlan
